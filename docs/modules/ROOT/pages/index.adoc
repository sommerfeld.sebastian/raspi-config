= RasPi Config Startpage
:url-project: https://gitlab.com/sommerfeld.sebastian/raspi-config
:url-ci-pipelines: {url-project}/pipelines
:img-ci-status: {url-project}/badges/master/pipeline.svg

This project provides scripts to set up my Raspberry Pi nodes (following a configuration as code approach). Just install the base image of choice (mostly Ubuntu) and run the respective scripts to provision the node.

image:{img-ci-status}[CI Status (GitLab CI), link={url-ci-pipelines}]

[cols="1,4", options="header"]
|===
|What |Where to find
|Repository |+++<i class="fab fa-gitlab"></i>+++ {url-project}
|Documentation |+++<i class="far fa-file-alt"></i>+++ https://docs-central.gitlab.io/docs-page/raspi-config-docs/index.html
|===

== How it works
For detailed information take a look at the respective setup page (for example xref:setups/ubuntu-21-04-server.adoc[Ubuntu Server 21.04 (RPi 3/4/400) 64-bit]).

=== Prerequisites
. RasPi and SD card hardware
. Install tool to write OS to SD card on your local machine (not the RasPi)

=== Use
To provision a Raspberry Pi node with basic software, configure users and ensure all RasPi nodes are handles the same way, follow these steps.

. Write Linux OS to SD card -> Distro of choice should be Ubuntu (see below)
. Run scripts from _src/main/setup/prepare-sd-card_ folder on your local machine prepare additional tools and scripts
. Insert SD card into RasPi and finish wizard (if needed)
. SSH into RasPi node and run tools from _/opt/node-provisioning_ to setup software on the RasPi
. Run scripts from _src/main/setup/utils_ folder on your normal workstation for additional configurations
. Use RasPi node as you wish

For further software installations / tasks, additional setup steps might be necessary. Each node (or cluster of nodes) comes with its own set of services and capabilities. For futher setup information check the respective node documentation (for example xref:nodes/prometheus.adoc[Prometheus]).

=== Raspberry Pi OS vs Ubuntu
Raspberry Pi OS seems more lightweight but in my experience Raspberry Pi OS is a little unstable and hence not suited to run 24/7. Ubuntu seems more stable. Ubuntu Desktop on the other hand is resource hungry and does not feel smooth. So Ubuntu Server with a lightweight XFCE desktop is the setup of choice if a graphical interface is needed.

== Report Bugs
Gitlab.com's Service Desk feature is used to handle bugs. To report bugs, just mail to incoming+sommerfeld-sebastian-raspi-config-26677383-issue-@incoming.gitlab.com.

== Contact
You can contact me via sommerfeld.sebastian@gmail.com.

== Links / Further information

. link:https://raspberrytips.com/create-image-sd-card[How to Create an Image of a Raspberry Pi SD Card? (Win/Linux/Mac)]