= RasPi Node: Prometheus

This RasPi node provides monitoring services and observability capabilities for my home network.

[cols="1,3", options="header"]
|===
|Metric |Value
|Hostname |prometheus
|RasPi Model |RasPi 4B -> 8GB
|Setup |xref:setups/ubuntu-21-04-server.adoc[Ubuntu Server 21.04 (RPi 3/4/400) 64-bit]
|===

== Software running on this RasPi Node
[cols="1,1,1,2,2", options="header"]
|===
|Software |Setup Type |Port |Info |Repo
|Prometheus + Grafana |Docker Compose |9090+++<br/>+++3000 |Monitoring Service for my home network -> all RasPi nodes report to this service+++<br/><br/>+++Start and stop scripts at `/home/pi/work/repos/prometheus-grafana/src/main` |https://gitlab.com/sommerfeld.sebastian/prometheus-grafana
|===

include::{partialsdir}/common-containers.adoc[]