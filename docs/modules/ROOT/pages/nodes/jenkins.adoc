= RasPi Node: Jenkins

This RasPi node runs the link:https://gitlab.com/sommerfeld.sebastian/docker-jenkins[docker-jenkins] master for automated ans scheduled builds.

[cols="1,3", options="header"]
|===
|Metric |Value
|Hostname |jenkins
|RasPi Model |RasPi 4B -> 8GB
|Setup |xref:setups/ubuntu-21-04-server.adoc[Ubuntu Server 21.04 (RPi 3/4/400) 64-bit]
|===

== Software running on this RasPi Node
[cols="1,1,1,2,2", options="header"]
|===
|Software |Setup Type |Port |Info |Repo
|Jenkins |Docker Compose |9080+++<br/>+++9081 |Jenkins Master+++<br/>+++Slave agent port+++<br/><br/>+++Start and stops script at `/home/pi/work/repos/raspi-config/src/main/jenkins` |https://gitlab.com/sommerfeld.sebastian/docker-jenkins
|===

include::{partialsdir}/common-containers.adoc[]

== Infrastructure
[plantuml, puml-build-image, svg]
----
@startuml
skinparam linetype ortho
skinparam monochrome false
skinparam componentStyle uml2
skinparam backgroundColor transparent
skinparam ArrowColor black
skinparam ComponentBorderColor black
skinparam NoteBorderColor Grey
skinparam NoteBackgroundColor #fdfdfd
skinparam defaultTextAlignment center
skinparam activity {
  FontName Ubuntu
}

component m as "Jenkins Master" <<ShuttlePC: "shuttle">>
component s as "Jenkins Slave" <<RasPi: "THE_NAME">>

m -> s

@enduml
----
