= RasPi Kubernetes Cluster (k3s)

link:https://k3s.io[K3s] is a highly available, certified Kubernetes distribution designed for production workloads in unattended, resource-constrained, remote locations or inside IoT appliances. Easy to install, half the memory, all in a binary of less than 100 MB.

Optimized for ARM: Both ARM64 and ARMv7 are supported with binaries and multiarch images available for both. K3s works great from something as small as a Raspberry Pi to an AWS a1.4xlarge 32GiB server.

This RasPi Kubernetes cluster utilizes k3s to build a Kubernetes platform on multiple Raspberry Pi nodes. This cluster is used as a platform to run a multitude of applications.

== Nodes
=== RasPi Node: k3s-master
[cols="1,3", options="header"]
|===
|Metric |Value
|Hostname |k3s-master
|RasPi Model |RasPi 4B -> 8GB
|Setup |xref:setups/ubuntu-21-04-server.adoc[Ubuntu Server 21.04 (RPi 3/4/400) 64-bit]
|===

=== RasPi Node: k3s-worker-1
[cols="1,3", options="header"]
|===
|Metric |Value
|Hostname |k3s-worker-1
|RasPi Model |RasPi 4B -> 8GB
|Setup |xref:setups/ubuntu-21-04-server.adoc[Ubuntu Server 21.04 (RPi 3/4/400) 64-bit]
|===

=== RasPi Node: k3s-worker-2
[cols="1,3", options="header"]
|===
|Metric |Value
|Hostname |k3s-worker-2
|RasPi Model |RasPi 4B -> 8GB
|Setup |xref:setups/ubuntu-21-04-server.adoc[Ubuntu Server 21.04 (RPi 3/4/400) 64-bit]
|===

== Services running in this cluster
=== Applications running in Kubernetes
[cols="1,3,>1", options="header"]
|===
|Application |Info |Port
|Dashboard |URL = http://k3s-master:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/ +++<br/>+++ Access Dashboard with bearer token copied from console output or _target/k3s-dashboard.token_. |8001 (needs proxy)
|Metrics-Server |Expose metrics to use with Prometheus + Grafana |??
|Jenkins |Image = registry.gitlab.com/sommerfeld.sebastian/docker-jenkins/main:latest +++<br/>+++ URL = http://k3s-master:30888+++<br/>+++ User (Password) = admin (admin) |30888
|===

include::{partialsdir}/common-containers.adoc[]

All data from Prometheus exporters is used by xref:nodes/prometheus.adoc[Prometheus RasPi node].

== Setup and Administration Guide
IMPORTANT: *TODO* -> Link to information in v-kube-cluster docs for virtual k3s

=== How to install k3s on master-node
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

.Links
. Docs -> https://rancher.com/docs/k3s/latest/en/
. Blog Post -> https://blog.alexellis.io/test-drive-k3s-on-raspberry-pi/
. Blog Post -> https://www.padok.fr/en/blog/raspberry-kubernetes
. Blog Post -> https://opensource.com/article/20/3/kubernetes-raspberry-pi-k3s
. Blog Post -> https://lookslikematrix.de/raspberry-pi/2020/09/27/k3s.html

=== How to install k3s on worker-node and join node to cluster
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

=== How to deploy applications
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

=== Ingress Setup
In Kubernetes, an Ingress is an object that allows access to your Kubernetes services from outside the Kubernetes cluster. You configure access by creating a collection of rules that define which inbound connections reach which services. NodePort and LoadBalancer let you expose a service by specifying that value in the service’s type. Ingress, on the other hand, is a completely independent resource to your service. You declare, create and destroy it separately to your services.

This makes it decoupled and isolated from the services you want to expose. It also helps you to consolidate routing rules into one place. Ingress may provide load balancing, SSL termination and name-based virtual hosting.

* link:https://kubernetes.io/docs/concepts/services-networking/ingress[Kubernetes Docs: Ingress]
* link:https://www.youtube.com/watch?v=NPFbYpb0I7w[Youtube: Kubernetes Ingress in 5 mins]
* https://matthewpalmer.net/kubernetes-app-developer/articles/kubernetes-ingress-guide-nginx-example.html[Blog: Kubernetes Ingress with Nginx Example]

image:k3s-cluster/kubernetes-ingress.png[]
