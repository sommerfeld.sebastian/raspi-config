* xref:index.adoc[]
* Setups
** xref:setups/ubuntu-21-04-server.adoc[]
** xref:setups/ubuntu-21-04-server-xfce.adoc[]
* Nodes
** xref:nodes/k3s-cluster.adoc[Kubernetes Cluster (k3s)]
** xref:nodes/jenkins.adoc[Jenkins]
** xref:nodes/prometheus.adoc[Prometheus]
