=== Common Containers
The setup uses Node-Exporter and cAdvisor to use as data sources for Prometheus and Grafana. These containers run on all RasPi nodes. All exporters run in containers.

Start and stop scripts at `/home/pi/work/repos/raspi-config/src/main/common/containers`.

[cols="1,3,>1", options="header"]
|===
|Container |Info |Port
|Node Exporter |OS metrics for Prometheus |9100
|cAdvisor |Container metrics for Prometheus |9110
|Portainer |Control Docker containers -> User (password) = admin (admin) |9990
|===
