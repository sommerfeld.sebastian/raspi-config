[cols="1,3", options="header"]
|===
|Category |Info
|OS |Ubuntu Server 21.04 (RPi 3/4/400) 64-bit
|Required SD Card |32 GB
|Recommended Model |RasPi 4B -> 4GB or more
|Default User (Password) |pi (raspberry) _-> preferred user_, ubuntu (raspberry)
|Default hostname |ubuntu-pi
|===