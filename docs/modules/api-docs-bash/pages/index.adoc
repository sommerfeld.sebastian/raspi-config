= API Docs: Bash Scripts
Jenkins Pipeline <sebastian@sommerfeld.io>

The full package documentation for all bash scripts of this project. All scripts are written for the link:https://en.wikipedia.org/wiki/Bash_(Unix_shell)[Bash] shell.

All scripts expect certain variables to be present. These variables are all in conjunction with coloring the console output. To use the variables, simply add these lines to your users .bashrc file.

[source, bash]
----
export LOG_DONE="[\e[32mDONE\e[0m]"
export LOG_ERROR="[\e[1;31mERROR\e[0m]"
export LOG_INFO="[\e[34mINFO\e[0m]"
export LOG_WARN="[\e[93mWARN\e[0m]"

export Y="\e[93m" # yellow
export P="\e[35m" # pink
export D="\e[0m"  # default (= white)
----

== Bash Scripts
// From this point down: generated content only ...

* xref:src_main_common_containers_logs.adoc[src/main/common/containers/logs.sh]
* xref:src_main_common_containers_start.adoc[src/main/common/containers/start.sh]
* xref:src_main_common_containers_stop.adoc[src/main/common/containers/stop.sh]
* xref:src_main_nodes_jenkins_clone.adoc[src/main/nodes/jenkins/clone.sh]
* xref:src_main_nodes_jenkins_start-jenkins.adoc[src/main/nodes/jenkins/start-jenkins.sh]
* xref:src_main_nodes_jenkins_stop-jenkins.adoc[src/main/nodes/jenkins/stop-jenkins.sh]
* xref:src_main_setup_node-provisioning_clone-repos.adoc[src/main/setup/node-provisioning/clone-repos.sh]
* xref:src_main_setup_node-provisioning_common_functions.adoc[src/main/setup/node-provisioning/common/functions.sh]
* xref:src_main_setup_node-provisioning_common_system-info.adoc[src/main/setup/node-provisioning/common/system-info.sh]
* xref:src_main_setup_node-provisioning_common_ubuntu.adoc[src/main/setup/node-provisioning/common/ubuntu.sh]
* xref:src_main_setup_node-provisioning_ubuntu-21-04-server-xfce.adoc[src/main/setup/node-provisioning/ubuntu-21-04-server-xfce.sh]
* xref:src_main_setup_node-provisioning_ubuntu-21-04-server.adoc[src/main/setup/node-provisioning/ubuntu-21-04-server.sh]
* xref:src_main_setup_node-provisioning_utils_change-hostname.adoc[src/main/setup/node-provisioning/utils/change-hostname.sh]
* xref:src_main_setup_node-provisioning_utils_create-ssh-keys.adoc[src/main/setup/node-provisioning/utils/create-ssh-keys.sh]
* xref:src_main_setup_node-provisioning_utils_slack_send-simple-message.adoc[src/main/setup/node-provisioning/utils/slack/send-simple-message.sh]
* xref:src_main_setup_prepare-sd-card_prepare-ubuntu-21-04-server-xfce.adoc[src/main/setup/prepare-sd-card/prepare-ubuntu-21-04-server-xfce.sh]
* xref:src_main_setup_prepare-sd-card_prepare-ubuntu-21-04-server.adoc[src/main/setup/prepare-sd-card/prepare-ubuntu-21-04-server.sh]
* xref:src_main_setup_utils_ssh-without-password.adoc[src/main/setup/utils/ssh-without-password.sh]
