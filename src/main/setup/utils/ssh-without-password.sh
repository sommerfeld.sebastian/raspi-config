#!/bin/bash

################################################################################
#                                                                              #
#    run on kobol - DO NOT RUN ON RASPBERRY PI NODES                           #
#                                                                              #
################################################################################

clear

################################################################################

case $HOSTNAME in
  ("kobol") echo -e "$LOG_INFO Setup SSH connections without password prompt";;
  (*)       echo -e "$LOG_ERROR DON'T RUN THIS SCRIPT ON RASPI NODE !!!" && echo && exit;;
esac

################################################################################

host="$1"

################################################################################

if [ -z "$host" ]
then
  echo -e "$LOG_ERROR Param missing: target hostname (string)"
  echo -e "$LOG_ERROR exit"
  exit 0
fi

################################################################################

key="/home/$USER/.ssh/raspi-$host.key"
echo -e "$LOG_INFO Create SSH key $key"
ssh-keygen -f "$key"

echo -e "$LOG_INFO Copy SSH key to $host using default username 'pi'"
ssh-copy-id "pi@$host"

echo -e "$LOG_DONE Created keys raspi-$host.key and raspi-$host.key.pub"
echo -e "$LOG_DONE SSH connection without password prompt for $host complete"
