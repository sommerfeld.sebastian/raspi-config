#!/bin/bash

################################################################################
#                                                                              #
#    Copied to RasPi node by util script                                       #
#    Invoke on RasPi node (using ssh connection)                               #
#                                                                              #
################################################################################

clear

echo "[INFO] Start provisioning of this RasPi node"

################################################################################

case $HOSTNAME in
  ("kobol") echo "[ERROR] DON'T RUN THIS SCRIPT ON YOUR NORMAL WORKSTATION !!!" && echo && exit;;
  (*)       echo "[INFO] Running on host '$(uname -n)'";;
esac

################################################################################

bash common/functions.sh
bash common/system-info.sh
bash common/ubuntu.sh

################################################################################

echo "[DONE] ========================================================================="
echo "[DONE] Finished provisioning of this RasPi node"
echo "[DONE] ========================================================================="

echo "[INFO] RasPi node will restart ..."
wait

sudo reboot
