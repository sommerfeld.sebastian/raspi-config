#!/bin/bash

################################################################################
#                                                                              #
#    Clone list for each raspi node                                            #
#    Clone repositories into /home/pi/work/repos                               #
#                                                                              #
################################################################################

if [ "$HOSTNAME" = "ubuntu" ]; then
  echo -e "$LOG_ERROR Don't clone yet! Finish node provisioning first!"
  echo -e "$LOG_ERROR exit"
  exit
elif [ "$HOSTNAME" = "prometheus" ]; then

  #
  # The clone list
  #
  repos=(
    'sommerfeld.sebastian/prometheus-grafana'
    #'sommerfeld.sebastian/portainer'
  )

fi

################################################################################

# shellcheck disable=SC2128
if [ -z "$repos" ]; then
  echo -e "$LOG_ERROR Repository list empty"
  echo -e "$LOG_ERROR Check if hostname '$HOSTNAME' is taken into account by this script"
  echo -e "$LOG_ERROR exit"
  exit
fi

################################################################################

(
  cd "$HOME/work/repos" || exit

  # clone repos
  for repo in "${repos[@]}"; do
    folder=$(cut -d "/" -f2 <<< "$repo") # get repo name without group name
    echo -e "$LOG_INFO Remove repo '$folder' (delete directory)"
    rm -rf "$folder"

    echo -e "$LOG_INFO Clone $repo"
    git clone "git@gitlab.com:$repo.git"
  done

  echo -e "$LOG_DONE Cloned all repos for $HOSTNAME"
)
