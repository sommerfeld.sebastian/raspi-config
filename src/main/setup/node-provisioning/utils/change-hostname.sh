#!/bin/bash

echo -e "$LOG_INFO Change hostname"

################################################################################

hostName="$1"

################################################################################

if [ -z "$hostName" ]
then
  echo -e "$LOG_ERROR Param for hostname missing (string without blanks please)"
  echo -e "$LOG_ERROR exit"
  exit 0
fi

################################################################################

echo -e "$LOG_INFO Change hostname to '$hostName'"
hostnamectl set-hostname "$hostName"
echo -e "$LOG_INFO ========== hostnamectl =================================================="
hostnamectl
echo -e "$LOG_INFO ========================================================================="
echo -e "$LOG_DONE Changed hostname"
