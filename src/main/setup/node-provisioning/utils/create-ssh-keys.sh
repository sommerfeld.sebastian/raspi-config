#!/bin/bash

######################################################################################
#    needed keys                                                                     #
#      Github.com                                                                    #
#      Gitlab.com                                                                    #
######################################################################################

echo -e "$LOG_INFO create ssh keys"

################################################################################

keyName="$1"
keyFile="/home/$USER/.ssh/$keyName.key"

################################################################################

if [ -z "$keyName" ]
then
  echo -e "$LOG_ERROR Param missing: key name (string without blanks)"
  echo -e "$LOG_ERROR exit"
  exit 0
fi

################################################################################

echo -e "$LOG_INFO Create new ssh key: $keyName"
cd ~/.ssh || exit
ssh-keygen -f "$keyFile" -t ed25519 -C "sommerfeld.sebastian@gmail.com"

echo -e "$LOG_INFO Start the ssh-agent in background"
eval "$(ssh-agent -s)"

echo -e "$LOG_INFO Add ssh key to the ssh-agent"
ssh-add "$keyFile"

echo -e "$LOG_DONE Key created"
