#!/bin/bash

#
# alias from .bashrc
#   slack-send-simple-msg
#

text="$1"

if [ -z "$text" ]
then
  echo -e "$LOG_ERROR Cannot send slack message - Mandatory param 'messageText' missing"
else
  echo -e "$LOG_INFO Send simple Message to Slack channel 'workstations' of workspace 'kobol'"

  curl --location --request POST "https://hooks.slack.com/services/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" \
  --header "Content-type:  application/json" \
  --data-raw "{
      'text': '$1',
      'blocks': [
        {
          'type': 'section',
          'text': {
            'type': 'mrkdwn',
            'text': '*$1*'
          }
        }
      ]
  }"

  echo
  echo -e "$LOG_DONE Simple slack message is sent"
fi
