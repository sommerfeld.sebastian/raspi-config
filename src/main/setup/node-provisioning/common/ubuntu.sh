#!/bin/bash

################################################################################
#                                                                              #
#    Copied to RasPi node by util script                                       #
#    DON'T RUN ON ITS ONW! RUN NODE SETUP SCRIPT                               #
#                                                                              #
################################################################################

bashrc="$HOME/.bashrc"

################################################################################

# hostname
echo "[INFO] Change hostname to 'ubuntu-pi'"
if [ "$HOSTNAME" = "ubuntu" ]; then
  echo "[INFO] RasPi node carries default hostname 'ubuntu'"
  echo "[INFO] Change hostname to 'ubuntu-pi'"
  hostnamectl set-hostname "ubuntu-pi"
else
  echo "[INFO] RasPi nodes hostname already changed to '$HOSTNAME'"
  echo "[INFO] Change of hostname is skipped"
fi
echo "[DONE] Changed hostname"

wait

# prompt color
promptDefinition="\${debian_chroot:+(\$debian_chroot)}\[\033[01;36m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ "
grep -qxF "export PS1='${promptDefinition}'" "$bashrc" || echo "export PS1='${promptDefinition}'" >>"$bashrc"
echo "[DONE] Changed prompt"

# welcome message
sudo cp resources/motd.txt /etc/motd
echo "[DONE] Set welcome message for ssh connections"

# aliases in .bashrc
aliases=(
  'alias ll="ls -alFh --color=auto"'
  'alias ls="ls -a --color=auto"'
  'alias grep="grep --color=auto"'
  'alias pull-all-repos="git all pull"'
  "cheatsheet() { clear && curl \"cheat.sh/\$1\" ; }"
  'export LOG_DONE="[\e[32mDONE\e[0m]"'
  'export LOG_ERROR="[\e[1;31mERROR\e[0m]"'
  'export LOG_INFO="[\e[34mINFO\e[0m]"'
  'export LOG_WARN="[\e[93mWARN\e[0m]"'
  'export Y="\e[93m"' # text yellow
  'export D="\e[0m"' # text default (white)
  'alias slack-send-msg="/opt/node-provisioning/utils/slack/send-simple-message.sh"'
)

for alias in "${aliases[@]}"; do
  grep -qxF "$alias" "$bashrc" || echo "$alias" >> "$bashrc"
done
echo "[DONE] Add aliases to .bashrc"

wait

################################################################################

echo "[INFO] Install common packages"

sudo apt-get -y update
sudo apt-get -y upgrade
echo "[DONE] update + upgrade"

wait

sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
echo "[DONE] Allow apt to use a repository over HTTPS"

wait

sudo apt-get install -y ncdu
sudo apt-get install -y htop
sudo apt-get install -y net-tools
sudo apt-get install -y console-data # keyboard layout
sudo ln -s /usr/bin/python3.9 /usr/bin/python # symlink to python shipped with ubuntu
echo "[DONE] Installed software"

wait

# git
sudo apt-get install -y git
git config --global alias.all "!f() { ls | xargs -I{} git -C {} \$1; }; f" # git pull for all repos in folder
git config --global user.email "sebastian@sommerfeld.io"
git config --global user.name "sebastian"
git config --global core.excludesfile ~/.gitignore           # push .gitignore files as well
git config --global credential.helper cache                  # Set git to use the credential memory cache
git config --global credential.helper "cache --timeout=3600" # Set the cache to timeout after 1 hour (setting is in seconds)
git config --global pull.rebase false                        # Checkout strategy -> use merge (wich is default anyway)
echo "[DONE] Installed git"

# git ssh config
sshKeyName="id_gitlab"
configFile="$HOME/.ssh/config"
if [ -f "$HOME/.ssh/$sshKeyName.key" ]; then
  echo "[INFO] Key pair '$HOME/.ssh/$sshKeyName.key' and '$HOME/.ssh/$sshKeyName.key.pub' already present"
  echo "[DONE] Skipping create ssh keys"
else
  echo "[INFO] Create ssh key '$sshKeyName.key' and '$sshKeyName.key.pub' for gitlab"
  bash utils/create-ssh-keys.sh "$sshKeyName"
  echo "[INFO] Configure git to use new ssh key"
  sudo rm "$configFile"
  sudo tee -a "$configFile" > /dev/null <<EOT
Host gitlab.com
 HostName gitlab.com
 IdentityFile ~/.ssh/$sshKeyName.key
EOT
  sudo chmod 644 "$configFile"
  sudo chown pi:pi "$configFile"
  echo "[DONE] Configured git with ssh keys"
fi

wait

# docker + docker compose
sudo apt-get install -y docker.io
sudo apt-get install -y docker-compose
sudo usermod -aG docker "$USER"
echo "[DONE] Docker and Docker Compose installed"

wait

sudo apt-get install -y openjdk-16-jre # java
sudo apt-get install -y maven
echo "[INFO] Initialize Maven"
mvn > /dev/null
echo "[DONE] Java and Maven installed"

wait

sudo apt-get install -y jq # handle json in bash
echo "[DONE] Installed jq"

wait

echo "[INFO] On Ubuntu Server an SSH client is already installed"
echo "[DONE] Common Ubuntu packages all installed"

################################################################################

mkdir "$HOME/.ssh"
chmod 700 "$HOME/.ssh"
mkdir "$HOME/tmp"
mkdir "$HOME/work"
mkdir "$HOME/work/repos"
mkdir "$HOME/work/servers"
ln -s "$HOME/.m2/repository" "$HOME/work/maven-repo"
echo "[DONE] Created default folders and symlinks"

################################################################################

(
  echo "[INFO] Clone 'raspi-config' into $HOME/work/repos"
  cd "$HOME/work/repos" || exit
  git clone git@gitlab.com:sommerfeld.sebastian/raspi-config.git
  echo "[INFO] Cloned 'raspi-config'"
)
