#!/bin/bash

################################################################################
#                                                                              #
#    Copied to RasPi node by util script                                       #
#    DON'T RUN ON ITS ONW! RUN NODE SETUP SCRIPT                               #
#                                                                              #
################################################################################

echo
echo "[INFO] ========== System Info =================================================="
echo "[INFO] hardware architecture ... $(uname -m)"
echo "[INFO] processor type .......... $(uname -p)"
echo "[INFO] plattform ............... $(uname -i)"
echo "[INFO] kernel version .......... $(uname -v)"
echo "[INFO] hostname ................ $(uname -n)"
echo "[INFO] ========== hostnamectl =================================================="
hostnamectl
echo "[INFO] ========================================================================="
echo
