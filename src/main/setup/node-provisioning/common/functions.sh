#!/bin/bash

wait() {
  interval=10
  echo "[INFO] Waiting for $interval seconds ..."
  sleep $interval
  echo "[INFO] Continuing ..."
}