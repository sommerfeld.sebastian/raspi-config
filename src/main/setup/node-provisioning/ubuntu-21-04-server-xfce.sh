#!/bin/bash

################################################################################
#                                                                              #
#    Copied to RasPi node by util script                                       #
#    Invoke on RasPi node (using ssh connection)                               #
#                                                                              #
################################################################################

clear

echo "[INFO] Start provisioning of this RasPi node"

################################################################################

case $HOSTNAME in
  ("kobol") echo "[ERROR] DON'T RUN THIS SCRIPT ON YOUR NORMAL WORKSTATION !!!" && echo && exit;;
  (*)       echo "[INFO] Running on host '$(uname -n)'";;
esac

################################################################################

bash common/functions.sh
bash common/system-info.sh
bash common/ubuntu.sh

################################################################################

sudo apt-get install -y xubuntu-desktop
echo "[DONE] Installed XFCE desktop environment"

wait

sudo apt-get install -y vlc # vlc
sudo apt-get install -y vlc-plugin-access-extra libbluray-bdj libdvdcss2 vlc-plugin-fluidsynth vlc-plugin-jack # vlc
sudo apt-get install -y ffmpeg
sudo apt-get install -y chromium-browser chromium-browser-l10n chromium-codecs-ffmpeg
sudo apt-get install -y stacer
echo "[DONE] Installed desktop software"

################################################################################

wait

sudo apt-get purge -y gimp
sudo apt-get purge -y thunderbird
sudo apt-get purge -y rhythmbox
sudo apt-get purge -y libreoffice*
sudo apt-get purge -y totem*
sudo apt-get purge -y apache2*
sudo apt-get purge -y remmina*
sudo apt-get purge -y gnome-tweak-tool gnome-tweaks
sudo apt-get -y clean
sudo apt-get -y autoremove

################################################################################

echo "[DONE] ========================================================================="
echo "[DONE] Finished provisioning of this RasPi node"
echo "[DONE] ========================================================================="

echo "[INFO] RasPi node will restart ..."
sleep 10

sudo reboot
