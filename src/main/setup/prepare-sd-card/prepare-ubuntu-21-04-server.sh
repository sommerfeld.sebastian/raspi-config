#!/bin/bash

################################################################################
#                                                                              #
#    run on kobol - DO NOT RUN ON RASPBERRY PI NODES                           #
#                                                                              #
################################################################################

clear

################################################################################

case $HOSTNAME in
  ("kobol") echo -e "$LOG_INFO Prepare SD card for RasPi node";;
  (*)       echo -e "$LOG_ERROR DON'T RUN THIS SCRIPT ON RASPI NODE !!!" && echo -e "$LOG_ERROR exit" && echo && exit;;
esac

if [ ! -f "resources/secrets/wifi.passwd" ]; then
    echo -e "$LOG_ERROR No wifi password file found"
    echo -e "$LOG_ERROR exit"
    exit
fi


################################################################################
#    check if SD card is present                                               #
################################################################################

boot_dir="/media/$USER/system-boot"
writable_dir="/media/$USER/writable"

array=(
  "$boot_dir"
  "$writable_dir"
)
for i in "${array[@]}"
do
	if [[ ! -d $i ]]; then
    echo -e "$LOG_ERROR sd card directory '$i' does not exist"
    echo -e "$LOG_ERROR exit"
    exit 0
  fi
done

################################################################################
#    start preparing SD card                                                   #
################################################################################

echo -e "$LOG_INFO Read wifi password"
pass=$(<"resources/secrets/wifi.passwd")
wifiPass=${pass%%*( )} # trim witespaces
wifiSSID="FRITZ!Box 6490 Cable"

echo -e "$LOG_INFO Write wifi information to SD card"
sudo rm "$boot_dir/network-config"
sudo tee -a "$boot_dir/network-config" > /dev/null <<EOT
version: 2
ethernets:
  eth0:
    dhcp4: true
    optional: true
wifis:
  wlan0:
    dhcp4: true
    optional: true
    access-points:
      "$wifiSSID":
        password: "$wifiPass"
EOT

echo -e "$LOG_INFO Copy provisioning scripts to SD card"
provisioning_dir="$writable_dir/opt/node-provisioning"
sudo rm -rf "$provisioning_dir"
sudo cp -r "../node-provisioning" "$provisioning_dir"

################################################################################

echo -e "$LOG_DONE SD card with headless operating system prepared"
