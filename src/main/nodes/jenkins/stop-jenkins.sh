#!/bin/bash
# @file stop-jenkins.sh
# @brief Stop Jenkins in container using the image build by xref:build.adoc[build.sh].
# @description The script stops the Jenkins docker container. After stopping Jenkins, the container is removed as well.
#
# NOTE: Sharing the socket only works for linux systems. Windows (and link:https://docs.microsoft.com/en-us/windows/wsl/about[WSL]) and Mac are not supported.
#
# ==== Arguments
#
# * *$1* (string): Determines which image to use. If set and equals 'latest', image 'registry.gitlab.com/sommerfeld.sebastian/docker-jenkins/main:latest' from remote registry is used. If empty, local image 'kobol/docker-jenkins:latest' is used. (optional)

export CONTAINER_NAME="jenkins-master"

docker stop "$CONTAINER_NAME"
docker rm "$CONTAINER_NAME"
