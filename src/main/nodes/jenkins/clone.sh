#!/bin/bash
# @file clone.sh
# @brief Clone relevant repositories.
# @description The script starts clones all relevant repositories from gitlab.com into /home/pi/work/repos directory. The directory is created if it does not exist. Needs provisioning of the raspberry pi node first.
#
# ==== Arguments
#
# The script does not accept any parameters.

reposDir="$HOME/work/repos"

echo -e "$LOG_INFO Create repos directory"
mkdir -p "$reposDir"

echo -e "$LOG_INFO Clone repos"
(
  cd "$reposDir" || exit
  git clone git@gitlab.com:sommerfeld.sebastian/docker-jenkins.git
)

echo -e "$LOG_DONE Finished repository cloning"
