#!/bin/bash
# @file start-jenkins.sh
# @brief Run Jenkins in container using the image build by xref:build.adoc[build.sh].
# @description The script starts Jenkins in docker with the previously built image for docker-jenkins master. The Jenkins UI is exposed on port 9080, the slave agent port on 9081. By sharing the unix socket "ssh auth sock" (mounted as volume) and setting the SSH_AUTH_SOCK environment variable, the container is able to use ssh keys from the host machine and hence is able to clone from and push to gitlab.com. To ensure the gitlab.com hosts are known, the $HOME/.ssh/known_hosts is mountet as well (it is assumed, that gitlab.com is known). Since the volumes are mounted at runtime, there is no need for ssh keys inside the container. The Container is started in background.
#
# NOTE: Sharing the socket only works for linux systems. Windows (and link:https://docs.microsoft.com/en-us/windows/wsl/about[WSL]) and Mac are not supported.
#
# ==== Arguments
#
# * *$1* (string): Determines which image to use. If set and equals 'latest', image 'registry.gitlab.com/sommerfeld.sebastian/docker-jenkins/main:latest' from remote registry is used. If empty, local image 'kobol/docker-jenkins:latest' is used. (optional)

export CONTAINER_NAME="jenkins-master"

# @description Initialize ssh agent before starting the container. Otherwise there is no agent running meaning the $SSH_AUTH_SOCK var is not set and hence the Jenkins container cannot use any ssh features. This is due to ssh agents only being started, when actually logging in locally, not connecting via ssh from a remote machine.
#
# @example
#    initSshAgent
function initSshAgent() {
  echo -e "$LOG_INFO Start ssh agent"
  eval  "$(ssh-agent -s)"
  echo -e "$LOG_INFO $SSH_AUTH_SOCK"
}

# @description Start docker container with given parameters. The function copies ssh keys and ssh information into the container and sets permissions properly, to allow Jenkins to clone from gitlab.com.
#
# @example
#    startContainers # local image
#    startContainers latest # image from remote registry
#
# @arg $1 string Image name
# @arg $2 string Tag (e.g. latest)
# @arg $3 integer Port for web UI
# @arg $4 integer Port for slave agent connect
function startContainers() {
  image="$1"
  tag="$2"
  portUi="$3"
  portAgents="$4"

  # Print information
  echo -e "$LOG_INFO ========== Variables ===================================================="
  echo -e "$LOG_INFO HOME .................. $HOME"
  echo -e "$LOG_INFO SSH_AUTH_SOCK  ........ $SSH_AUTH_SOCK"
  echo -e "$LOG_INFO image ................. $image"
  echo -e "$LOG_INFO tag ................... $tag"
  echo -e "$LOG_INFO port (ui) ............. $portUi"
  echo -e "$LOG_INFO port (slave agents) ... $portAgents"
  echo -e "$LOG_INFO ========================================================================="

  # Start container
  echo -e "$LOG_INFO Start Jenkins container"
  docker run -d --restart unless-stopped -p "$portUi:8080" -p "$portAgents:9081" \
    --volume "/var/run/docker.sock:/var/run/docker.sock" \
    --volume "$SSH_AUTH_SOCK:/run/user/1000/keyring/ssh" \
    --volume "/etc/timezone:/etc/timezone:ro" \
    --volume "/etc/localtime:/etc/localtime:ro" \
    --env "SSH_AUTH_SOCK=$SSH_AUTH_SOCK" \
    --env ARCH=arm \
    --name="$CONTAINER_NAME" \
    "$image:$tag"

#    --volume "$HOME/.ssh/authorized_keys:/root/.ssh/authorized_keys" \
#    --volume "$HOME/.ssh/config:/root/.ssh/config" \
#    --volume "$HOME/.ssh/gitlab.key:/root/.ssh/gitlab.key" \
#    --volume "$HOME/.ssh/gitlab.key.pub:/root/.ssh/gitlab.key.pub" \
#    --volume "$HOME/.ssh/known_hosts:/root/.ssh/known_hosts" \

    seconds=10
    echo -e "$LOG_INFO Wait $seconds seconds"
    sleep "$seconds"

    echo -e "$LOG_INFO Copy ssh information into container"
    docker exec -it jenkins-master mkdir -p /root/.ssh
    docker cp "$HOME/.ssh/authorized_keys" "$CONTAINER_NAME:/root/.ssh/authorized_keys"
    docker cp "$HOME/.ssh/config" "$CONTAINER_NAME:/root/.ssh/config"
    docker cp "$HOME/.ssh/gitlab.key" "$CONTAINER_NAME:/root/.ssh/gitlab.key"
    docker cp "$HOME/.ssh/gitlab.key.pub" "$CONTAINER_NAME:/root/.ssh/gitlab.key.pub"
    docker cp "$HOME/.ssh/known_hosts" "$CONTAINER_NAME:/root/.ssh/known_hosts"

    echo -e "$LOG_INFO Change owner and permissions"
    docker exec -it jenkins-master chmod 600 /root/.ssh/authorized_keys
    docker exec -it jenkins-master chown root:root /root/.ssh/authorized_keys
    docker exec -it jenkins-master chmod 600 /root/.ssh/known_hosts
    docker exec -it jenkins-master chown root:root /root/.ssh/known_hosts
    docker exec -it jenkins-master chmod 644 /root/.ssh/config
    docker exec -it jenkins-master chown root:root /root/.ssh/config
    docker exec -it jenkins-master chmod 600 /root/.ssh/gitlab.key
    docker exec -it jenkins-master chown root:root /root/.ssh/gitlab.key
    docker exec -it jenkins-master chmod 644 /root/.ssh/gitlab.key.pub
    docker exec -it jenkins-master chown root:root /root/.ssh/gitlab.key.pub
}

################################################################################

initSshAgent

echo -e "$LOG_INFO Run latest Jenkins from container registry at gitlab.com"
startContainers 'sommerfeld.jfrog.io/default-docker-local/docker-jenkins' 'nightly-arm' 9080 9081
